<?php

namespace app\controllers;

use Yii;
use app\models\Responsables;
use app\models\ResponsablesSearch;
use app\models\Alumnos;
use app\models\AlumnosSearch;
use yii\web\Controller;
use app\models\Tutores;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * ResponsablesController implements the CRUD actions for Responsables model.
 */
class ResponsablesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Responsables models.
     * @return mixed
     */
    public function actionIndex()
    {
        $alumno = $_GET['alumno'];
        $searchModel = new ResponsablesSearch();
        $modelTutores = new Tutores();
        $model = new Responsables();
      //enviamos los datos de los alumnos para mostrar en cabecera de responsables
        $query = Alumnos::find()
                ->select('dni,nombre,apellidos')
                ->where(['dni' => $alumno]);
                
        $datosAlumno =  new ActiveDataProvider([
            'query' => $query,
        ]);
         
       
       
       // $datosAlumno_arr = ArrayHelper::map(Responsables::getAlumno($alumno), 'dni', 'nombre','apellidos'); 
       
       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere("dni_alumno = '$alumno'");

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' =>$modelTutores,
            'dniAlumno' =>$alumno,
            'datosAlumno' =>$datosAlumno,
           
        ]);
    }

    /**
     * Displays a single Responsables model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Responsables model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Responsables();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Responsables model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Responsables model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        
        $this->findModel($id)->delete();
        return $this->redirect(['responsables/index','alumno'=>$_POST['alumno']]);
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Responsables model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Responsables the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Responsables::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
    
}
