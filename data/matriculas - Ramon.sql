USE matriculas;


DROP TABLE IF EXISTS discapacidades;
CREATE OR REPLACE TABLE discapacidades(
  id int(11) AUTO_INCREMENT,
  tipo varchar(200),
  PRIMARY KEY(id)
);

DROP TABLE IF EXISTS alumnos;
CREATE OR REPLACE TABLE alumnos(
  id int(11) AUTO_INCREMENT,
  codigo int(11),
  id_escolar int(11),
  expe_centro int(11),
  dni int(9),
  nombre varchar(100),
  apellidos varchar(200),  
  f_nac date,
  loc_nac varchar(100),
  prov_nac varchar(100),
  domicilio varchar(200),
  cp int(5),
  localidad varchar(100),
  provincia varchar(100),
  tel_fijo int(9),
  movil int(9),    
  discapacidad int(11) DEFAULT NULL,
  centro_ant varchar(200),
  tit_acceso varchar(200),        
  foto mediumblob,                                                       
  PRIMARY KEY(id),
  CONSTRAINT fk_alumnosDiscapa FOREIGN KEY (discapacidad) REFERENCES discapacidades(id)
);


DROP TABLE IF EXISTS ciclos;
CREATE OR REPLACE TABLE ciclos(
  id int(11) AUTO_INCREMENT,
  referencia varchar(20),
  denominacion varchar(200),
  reflegal1 varchar(200),
  reflegal2 varchar(200),
  PRIMARY KEY(id),
  UNIQUE(referencia)
);

DROP TABLE IF EXISTS modulos;
CREATE OR REPLACE TABLE modulos(
  id int(11) AUTO_INCREMENT,
  nombre varchar(300),
  PRIMARY KEY(id)
);

DROP TABLE IF EXISTS modulosciclo;
CREATE OR REPLACE TABLE modulosciclo(
  id int(11) AUTO_INCREMENT,
  id_ciclo int(11),
  id_modulo int(11),
  curso int(2),
  PRIMARY KEY(id),
  CONSTRAINT fk_modulocicloCiclo FOREIGN KEY (id_ciclo) REFERENCES ciclos(id),
  CONSTRAINT fk_modulocicloModulo FOREIGN KEY (id_modulo) REFERENCES modulos(id)

);

DROP TABLE IF EXISTS datosbancarios;
CREATE OR REPLACE TABLE datosbancarios(
  id int(11) AUTO_INCREMENT,
  iban varchar(24),
  titular varchar(200),
  dni int(9),
  PRIMARY KEY(id)
);

DROP TABLE IF EXISTS tutores;
CREATE OR REPLACE TABLE tutores(
  id int(11) AUTO_INCREMENT,
  nombre varchar(100),
  apellidos varchar(200),
  dni int(9),
  telefono int(9),
  PRIMARY KEY(id)

);
DROP TABLE IF EXISTS responsables;
CREATE OR REPLACE TABLE responsables(
  id int(11) AUTO_INCREMENT,
  id_tutor int(11),
  id_alumno int(11),
  PRIMARY KEY(id),
  CONSTRAINT fk_responablesTutores FOREIGN KEY (id_tutor) REFERENCES turores(id),
  CONSTRAINT fk_responablesAlumno FOREIGN KEY (id_alumno) REFERENCES alumnos(codigo)
                  //comprobar si el id-escolar o el exped corresponden con el cod de las matriculas
);



DROP TABLE IF EXISTS matriculas;
CREATE OR REPLACE TABLE matriculas(
  id int(11) AUTO_INCREMENT,
  id_alumno int(11),
  id_ciclo int(11),
  id_datos_bancarios int(11),
  fecha date,
  tipo varchar(50),
  abona_matricula tinyint (1),
  PRIMARY KEY(id),
  CONSTRAINT fk_matriculasAlumno FOREIGN KEY (id_alumno) REFERENCES alumnos(codigo),
  CONSTRAINT fk_matriculasCiclo FOREIGN KEY (id_ciclo) REFERENCES ciclos(id),
  CONSTRAINT fk_matriculasDatosBancarios FOREIGN KEY (id_datos_bancarios) REFERENCES datosbancarios(id)
);

DROP TABLE IF EXISTS modulosmatricula;
CREATE OR REPLACE TABLE modulosmatricula(
  id int(11) AUTO_INCREMENT,
  id_matricula int(11),
  id_modulo int(11),
  estado varchar(2),
  PRIMARY KEY(id),
  CONSTRAINT fk_modulosmatriculaMatriculas FOREIGN KEY (id_matricula) REFERENCES matriculas(id),
  CONSTRAINT fk_modulosmatriculaModulos FOREIGN KEY (id_modulo) REFERENCES modulosciclos(id)
);





                  

INSERT INTO ciclos VALUES('','AYF','Administración y Finanzas','Ref_legal_1 R.D. 1584/2011','Ref_legal_2 Orden ECD 85/2012');
INSERT INTO ciclos VALUES('','DAI','Desarrollo de Aplicaciones Informáticas','R.D. 1661/94','R.D. 1676/94');
INSERT INTO ciclos VALUES('','DAM ','Desarrollo de Aplicaciones Multiplataforma','R.D. 450/2010','Orden EDU/56/2011');
INSERT INTO ciclos VALUES('','DSA','Documentación y Administración Sanitarias','R.D.768/2014','Orden ECD 75/2015');
INSERT INTO ciclos VALUES('','FPB','Informática y Comunicaciones','R.D. 127/2014','Orden ECD 71/2014');
INSERT INTO ciclos VALUES('','GAD','Gestión Administrativa','R.R. 1126/2010','Orden EDU/82/2010');



