<?php

namespace app\models;

use Yii;

use yii\data\SqlDataProvider;

/**
 * This is the model class for table "matriculas".
 *
 * @property int $id
 * @property string|null $dni_alumno
 * @property int|null $id_ciclo
 * @property int|null $id_datos_bancarios
 * @property string|null $fecha
 * @property string|null $tipo
 * @property int|null $seguro
 * @property string|null $curso_academico
 *
 * @property Alumnos $dniAlumno
 * @property Ciclos $ciclo
 * @property Datosbancarios $datosBancarios
 * @property Modulosmatricula[] $modulosmatriculas
 */
class Matriculas extends \yii\db\ActiveRecord
{
    public $firma;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matriculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_ciclo', 'fecha','pago','curso_academico','tipo','curso'], 'required'],
            [['id_ciclo', 'id_datos_bancarios','pago', 'seguro','f_matricula','curso','repite','privez'], 'integer'],
            [['fecha'], 'safe'],
            [['dni_alumno'], 'string', 'max' => 9],
            [['tipo'], 'string', 'max' => 50],
            [['curso_academico'], 'string', 'max' => 5],
            [['dni_alumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['dni_alumno' => 'dni']],
            [['id_ciclo'], 'exist', 'skipOnError' => true, 'targetClass' => Ciclos::className(), 'targetAttribute' => ['id_ciclo' => 'id']],
            [['id_datos_bancarios'], 'exist', 'skipOnError' => true, 'targetClass' => Datosbancarios::className(), 'targetAttribute' => ['id_datos_bancarios' => 'id']],
            [['firma'], 'string'],
        ];
        


    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dni_alumno' => 'Dni',
            'id_ciclo' => 'Ciclo',
            'id_datos_bancarios' => 'Datos Bancarios',
            'pago' => 'Tipo de Pago',
            'fecha' => 'Fecha',
            'tipo' => 'Tipo',
            'seguro' => 'Seguro Matricula',
            'f_matricula' => 'Firma Matricula',
            'curso_academico' => 'Curso Academico',
            'curso' => 'Curso',
            'repite' => 'Repite',
            'privez' => '1ªVez',
        ];
    }

    /**
     * Gets query for [[DniAlumno]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniAlumno()
    {
        return $this->hasOne(Alumnos::className(), ['dni' => 'dni_alumno']);
    }
    
    public function getAlumno0()
    {
        return $this->hasOne(Alumnos::className(), ['dni' => 'dni_alumno']);
    }
     public function getCiclo0()
    {
        return $this->hasOne(Ciclos::className(), ['id' => 'id_ciclo']);
    }
    
     public function getAlumnos()
    {
        return $this->hasOne(Alumnos::className(), ['dni' => 'dni_alumno']);
    }

    /**
     * Gets query for [[Ciclo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCiclo()
    {
        return $this->hasOne(Ciclos::className(), ['id' => 'id_ciclo']);
    }

    /**
     * Gets query for [[DatosBancarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDatosBancarios()
    {
        return $this->hasOne(Datosbancarios::className(), ['id' => 'id_datos_bancarios']);
    }

    /**
     * Gets query for [[Modulosmatriculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModulosmatriculas()
    {
        return $this->hasMany(Modulosmatricula::className(), ['id_matricula' => 'id']);
    }
    
//        public function getResponsables()
//    {
//        return $this->hasMany(Responsables::className(), ['dni_alumno' => 'dni']);
//    }
    
    
     //creados para que coincidan las tablas con las relaciones en informe dinámico
     public function getCiclos()
    {
        return $this->hasOne(Ciclos::className(), ['id' => 'id_ciclo']);
    }
    
    //------------------------------------------------------------------------
    
    
    
    public function getMatriculaduplicada($alumno,$ciclo,$cursoacademico){
         $model = Matriculas::find()
                 ->where(['dni_alumno' => $alumno])
                 ->andWhere(['curso_academico' => $cursoacademico])
                 ->andWhere(['id_ciclo' => $ciclo])
                 ->all();

         
        if (count($model)>0) {
            return true;
        }
    }
    
    
    
  
    public function getGuardarimagenFirma(){
        
        $ruta_origen = '../web/img/';
        $carpeta = '../web/img/alumnos/';
        $ruta = '../web/img/alumnos/'.$this->dni_alumno.'/';
        $ruta_matricula = '../web/img/alumnos/'.$this->dni_alumno.'/'.$this->id."(".$this->curso_academico.")".'/';
        
        if(!file_exists($carpeta))
            $ruta_contratos = mkdir($carpeta,0755,true);
       
        if(!file_exists($ruta))
            $ruta_nif = mkdir($ruta,0755,true);   
            
        if(!file_exists($ruta_matricula))
            $carpeta_matricula = mkdir($ruta_matricula,0755,true);
            
           if($this->firma != Null){
              //comprobamos si ya existe una imagen de firma para esa matricula. si es así la borramos.
               $nombre_fichero = $ruta_matricula.'firma.png';
               if (file($nombre_fichero)) 
                  unlink($nombre_fichero);
             //creamos la imagen de firma digital que viene en base64 del modelo que despues subiremos a nuestro servidor
               
                file_put_contents($ruta_matricula.'/'.'firma.png', base64_decode(explode(',',$this->firma)[1]));
                
                //actualizamos a true la firma proteccion datos de la ficha del alumno. Ha firmado el doc.protección datos
               
                $connection = Yii::$app->db;
                $command = $connection->createCommand("UPDATE alumnos SET f_proteccion = 1 WHERE dni = '$this->dni_alumno'");
                $command->execute();
                    
                return true;
           }else{
               
                return false;
           }
    }
    
 
    
     public function afterFind() {
        parent::afterFind();
        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d-m-Y');;
        
    }
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:Y-m-d');
          return true;
    }
}
