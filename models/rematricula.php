<?php

namespace app\models;


use Yii;

/**
 * This is the model class for table "recibos".
 *
 * @property int $id
 * @property int $matricula
 * @property string $emision
 * @property int $mes
 * @property int $anyo
 * @property int $estado
 * @property int $reducido
 * @property double $importe
 *
 * @property Matriculas $matricula0
 */

 
 
class Rematricula extends Matriculas
{
    public $curso_acad;
    public $ciclo;
    public $curso;
    /**
     * {@inheritdoc}
     */
  

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['curso_acad', 'string'], 'required'],
            [['ciclo', 'integer'], 'required'],
            [['curso', 'integer'], 'required'],              
       ];        
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
         $labels = ['curso_acad' => 'Curso Académico', 'ciclo' => 'Ciclo Formativo', 'curso' => 'Curso'];
        return array_merge(parent::attributeLabels(), $labels);
       
    }

}
