<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Bancos;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Datosbancarios */
/* @var $form yii\widgets\ActiveForm */
$itemBancos = ArrayHelper::map(Bancos::find()->all(), 'nombre','nombre');
?>

<div class="datosbancarios-form">

    <?php $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'form_datos_bancarios']
    ]); ?>
    
     <?= $form->field($model, 'banco')->dropDownList($itemBancos, ['prompt' => '']); ?>

    <?= $form->field($model, 'iban')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'titular')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
