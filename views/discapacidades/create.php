<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Discapacidades */

$this->title = 'Nueva Discapacidad';
$this->params['breadcrumbs'][] = ['label' => 'Discapacidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discapacidades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
