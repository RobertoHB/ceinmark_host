<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        //'brandLabel' => Yii::$app->name,
        'brandLabel' => Html::img('@web/img/logoPie.jpg', ['alt' => Yii::$app->name, 'style' => 'height: 30px; width: 171px; top: 15px; left: 50px; position: absolute;']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
             ['label' => 'Registro', 'url' => ['/site/register'],'visible'=>!Yii::$app->user->isGuest],
            ['label' => 'Alumnos', 'url' => ['/alumnos'],'visible'=>!Yii::$app->user->isGuest],
            ['label' => 'Matriculas', 'url' => ['/matriculas'],'visible'=>!Yii::$app->user->isGuest],
            
            ['label' => 'Datos Bancarios', 'url' => ['#'],
                'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                                ['label' => 'Bancos', 'url' => ['/bancos']],
                                ['label' => 'Cuentas', 'url' =>['/datosbancarios']],
                           ],'visible'=>!Yii::$app->user->isGuest
                  ],
            
            
            ['label' => 'Ciclos/Modulos', 'url' => ['#'],
                'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                                ['label' => 'Ciclos', 'url' => ['/ciclos']],
                                ['label' => 'Modulos', 'url' =>['/modulos']],
                                ['label' => 'Modulos/Ciclo', 'url' =>['/modulosciclo']],
                               
                           ],'visible'=>!Yii::$app->user->isGuest
            ],
            ['label' => 'Informes', 'url' => ['#'],
                'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                                ['label' => 'Generador', 'url' => ['/site/generadorinformes']],
                                ['label' => 'Impresos', 'url' => ['/site/impresosblanco']],
                              
                           ],'visible'=>!Yii::$app->user->isGuest
             ],
            ['label' => 'Mantenimiento', 'url' => ['#'],
                'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                                ['label' => 'Rematriculaciones', 'url' => ['/matriculas/rematricula']],
                                                           
                           ],'visible'=>!Yii::$app->user->isGuest
             ],

         
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<!--<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
