<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Alumnos;
use dosamigos\datepicker\DatePicker;
use yii\helpers\Url;
use app\models\Ciclos;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MatriculasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$cursosAcademicos = ['18-19'=>'18-19','19-20'=>'19-20','20-21'=> '20-21','21-22' => '21-22','22-23' => '22-23','23-24'=>'23-24',
                    '24-25'=>'24-25','25-26'=>'25-26','26-27'=>'26-27','27-28'=>'27-28','28-29'=>'28-29','29-30'=>'29-30',
                    '30-31'=>'30-31', '31-32'=>'31-32', '32-33'=>'32-33', '33-34'=>'33-34', '34-35'=>'34-35', '35-36'=>'35-36',
                    '36-37'=>'36-37', '37-38'=>'37-38', '38-39'=>'38-39', '39-40'=>'39-40', '40-41'=>'40-41', '41-42'=>'41-42',
                    '42-43'=>'42-43','43-44'=>'43-44','44-45'=>'44-45','45-46'=>'45-46','46-47'=>'46-47','47-48'=>'47-48','48-49'=>'48-49',
                    '49-50'=>'49-50'];
$tipoMatricula = ['0'=>'Ordinaria','1'=>'Modular'];

$this->title = 'Matriculas';
$this->params['breadcrumbs'][] = $this->title;
//$cabecera = ucwords(strtolower($_REQUEST['nombre']))." ". ucwords(strtolower($_REQUEST['apellidos']))." "."(". ucwords(strtolower($_REQUEST['dni'])).")" ?>
 

<div class="matriculas-index">
      <h3>Alumno</h3>
              
    <?php           
    
    
        if(isset($_REQUEST['alumno'])){
         
           echo GridView::widget([
             'dataProvider' => $datosAlumno,
             'summary' => '',
             //'filterModel' => $searchModel,
             'columns' => [
                  [
                'attribute'=>'passnie',
                'contentOptions' => ['style' => 'width:10px; white-space: normal;text-align:center;'],
            ],
                
                 'dni',
                 'nombre',
                 'apellidos',
             ],
            ]);
        }
    ?>
  
    <p>
        <?php
           if(isset($_REQUEST['alumno'])){
               echo Html::a('Crear Matriculas', ['create','alumno'=>$dniAlumno],  
                ['class' => 'btn btn-success']); 
            }
        
        ?>
          
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            
           
                  [
               'label'=>'Matricula', 
               'attribute'=> 'id',
                'headerOptions' => ['style' => 'width:20px;']
            ],
           //'alumno',
             //'dni_alumno',
             [
             'label' => 'Pass/Nie',
              'attribute' => 'passnie',
              'value' => 'alumno0.passnie',
              'filter' => ArrayHelper::map(Alumnos::find()->all(), 'dni', 'passnie'),
              'headerOptions' => ['style' => 'width:50px'],
              'enableSorting' => true,
            ],
             [
                'label' => 'DNI',
                'attribute' => 'dni_alumno',
                'headerOptions' => ['style' => 'width:100px']
            ],
            [
              'label' => 'Nombre',
              'attribute' => 'alumno',
              'value' => 'alumno0.nombre',
              'filter' => ArrayHelper::map(Alumnos::find()->all(), 'dni', 'nombre','apellidos'),
              'headerOptions' => ['style' => 'width:75px'],
              'enableSorting' => true,
            ],
            [
              'label' => 'Apellidos',
              'attribute' => 'alumno',
              'headerOptions' => ['style' => 'width:175px'],
              'value' => 'alumno0.apellidos',
              'filter' => ArrayHelper::map(Alumnos::find()->all(), 'dni', 'nombre','apellidos'),
              'enableSorting' => true,
            ],
            [
                'label' => 'Ciclo',
                'attribute' => 'id_ciclo',
                'value' => 'ciclo0.denominacion',
                'headerOptions' => ['style' => 'width:200px'],
                'filter' => ArrayHelper::map(Ciclos::find()->all(), 'id', 'denominacion'),
                'enableSorting' => true,
                
            ],
              [
                'label' => 'Curso Acad.',
                'attribute' => 'curso_academico',
                'headerOptions' => ['style' => 'width:100px'],
                'filter' => ArrayHelper::toArray($cursosAcademicos),
                'enableSorting' => true,
                 
            ],
                  [
             'label'=>'Curso',
             'attribute'=>'curso',
             'format'=>'raw',
             'headerOptions' => ['style'=>'text-align:center;width:25px;'],
             'filter' => [1=>'1',2=>'2'],  
             'headerOptions' => ['style' => 'width:80px;'],
             
             //'contentOptions' => [0=>['style'=>'color:red'],1=>['style'=>'color:blue']],   
             'value' => function($model) {
                            return $model->curso == 1 ? '1' : '2';}             
            ],
//                 [
//                'attribute' => 'fecha',
//                'value'=>'fecha',
//                'format'=>'raw',
//                'headerOptions' => ['style' => 'width:175px;'],
//                'filter' => DatePicker::widget([
//                   'model'=>$searchModel,
//                   'attribute'=>'fecha',
//                    'inline' =>false,
//                    'clientOptions' => [
//                        'autoclose' => true,
//                        'format' => 'dd-mm-yyyy',
//                    ]  
//                ]),
                
//            ],
            [
                'label' => 'Tipo',
                'attribute' => 'tipo',
//                'value' => ArrayHelper::toArray($tipoMatricula),
                'headerOptions' => ['style' => 'width:120px'],
                'filter' => ArrayHelper::toArray($tipoMatricula),
              	'value' => function($model) {
                            return $model->tipo == 1 ? 'Modular' : 'Ordinaria';}
            ],
            [
             'label'=>'Seguro',
             'attribute'=>'seguro',
             'format'=>'raw',
             'headerOptions' => ['style'=>'text-align:center;width:25px;'],
             'contentOptions' => function ($model) {
                             return ['style' => 'color:' 
                                 .($model->seguro === 0 ? 'red' : 'green')];
                            },
             'filter' => [0=>'No',1=>'Si'],  
             'headerOptions' => ['style' => 'width:80px;'],
             
             //'contentOptions' => [0=>['style'=>'color:red'],1=>['style'=>'color:blue']],   
             'value' => function($model) {
                            return $model->seguro == 1 ? 'Si' : 'No';}             
            ],

//            [
//                'label' => 'Seguro',
//                'attribute' => 'abona_matricula',
//                'headerOptions' => ['style' => 'width:10px']
//            ],
           

            //'id_ciclo',
           // 'id_datos_bancarios',
           // 'fecha',
          //  'tipo',
            //'abona_matricula',
            //'curso_academico',
    
            ['class' => 'yii\grid\ActionColumn',
                
                'contentOptions' => ['style' => 'width:60px;'],
                'header'=>'Acciones',
                'template' => '{update}{delete}',
                'buttons' => [

                                'update' => function ($url,$model) {
                                    return Html::a(
                                    '<span class="glyphicon glyphicon-pencil"></span>', ['matriculas/update', 'id' =>$model->id,'alumno'=>$model->dni_alumno]
                                     // Url::to('@web/matriculas/update'.'?id='.$model->id),
                                      
                                      
                                        //['data'=>[
                                        //            'method' => 'get',
                                        //            'params'=>['id'=>$model->id,'alumno'=>$model->dni_alumno],                                         
                                        //            ]
                                            
                                        //    ]
                                    );
                                 },

                                'delete' => function ($url, $model, $key) {
                                    return  Html::a(
                                    '<span class="glyphicon glyphicon-trash" style="padding-left:5px;"></span>',['matriculas/delete','id'=>$model->id],                                                                
                                             ['data'=>[
                                                    'method' => 'post',
                                                    'confirm' => 'Are you sure?',
                                                    'params'=>['alumno'=>$model->dni_alumno], 
                                                    ]
                                            
                                            ]
                                    );

                                }

                            ]
            ],  
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
//            ['class' => 'yii\grid\ActionColumn',
//                  'contentOptions' => ['style' => 'width:70px;'],
//                    'header'=>'Acciones'],
//               [
//                'class' => 'yii\grid\ActionColumn',
//                'template' => '{update}',
//                'contentOptions'=>['style'=>'width: 30px;'],
//                'buttons' => [
//                'actualizar' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pen"></span>', $url, [
//                                        'title' => Yii::t('app', 'update'),
//                                    ]);
//
//                                }, 
//                                
//                ],
//                        
//                'urlCreator' => function ($action, $model, $key, $index) {
//                     return Url::to(['update', 'id' => $model->id,'alumno'=>$model->dni_alumno]);
//                }        
//                 
//            ],  
                    
          
           
        ],
    ]); ?>


</div>
