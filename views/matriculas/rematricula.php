<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
//use kartik\form\ActiveForm;
use yii\data\ActiveDataProvider;

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\swiftmailer;
//use kartik\export\ExportMenu;
//use kartik\grid\GridView;
use dosamigos\datepicker\DatePicker;
//use ArrayHelper;
use app\models\Matriculas;  
use app\models\Ciclos;

$this->title = 'Generador de Informes';
$this->params['breadcrumbs'][] = $this->title;
$curso_acad =  ['19-20'=>'19-20','20-21'=>'20-21','21-22'=>'21-22','22-23'=>'22-23',
                '23-24'=>'23-24','24-25'=>'24-25','25-26'=>'25-26','26-27'=>'26-27',
                '27-28'=>'27-28','28-29'=>'28-29','29-30'=>'29-30'];
$ciclo = ArrayHelper::map(Ciclos::find()->all(),'id', 'referencia','denominacion'); 
$curso = ['1'=>'1º','2'=>'2º'];





 
        
//        $itemsAlumnos = ArrayHelper::map(Alumnos::find()->asArray()->select('id,apellidos,nombre')->orderBy('apellidos')->all(),'id','apellidos','nombre');
     
//        $itemsAlumnos =  ArrayHelper::map(Alumnos::find()->select(['id','apellidos','nombre'])->asArray()->orderBy('apellidos')->all(),'id', function ($model) {
//                        return $model['apellidos'] .' - '. $model['nombre'];
//                        });


?>

<style>
    
   .contiene_check{
       padding-left:5px;
       text-align: left;
      
    }
    .item{
        font-size: 16px;
        margin-top: 5px;
        width:290px;
        
    }
    .tableFixHead          { overflow-y: auto; height:700px; }
    .tableFixHead thead th { position: sticky; top: 0; }

    /* Just common table stuff. Really. */
    table  { border-collapse: collapse; width: 100%;}
    th, td { padding: 2px 4px; }
    th     { background:#eee; }
    
    select {
    font-family: 'Lato', 'Font Awesome 5 Free';
    font-weight: 900;
    }
    .listado{
        margin-left:50px;
        width:100%;
    }
    #listado_pjax{
        margin-top:25px;
    }
    .opciones{
      border:1px solid blanchedalmond;
      border-radius: 20px;
      width:310px;
      margin-top:20px;
    }
    
</style>
    
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>

<div class="alumnos-index">
     <?php $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'filtros_rematriculas',
            'enableClientValidation' => true,
            'enableAjaxValidation' => false,
            'action'=>'rematricula',
           ]); ?>
   
        <!--<div class = "row"><h1><?= Html::encode($this->title) ?></h1>-->
    <div class="row">
        <div class="col col-sm-3 opciones">
            <div class="form-group" style="margin-top:15px;">   
                <?= $form->field($model, 'curso_acad')->dropDownList($curso_acad, ['prompt' => '','id'=>'curso_acad' ]); ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'ciclo')->dropDownList($ciclo, ['prompt' => '','id'=>'ciclo' ]); ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'curso')->dropDownList($curso, ['prompt' => '','id'=>'curso' ]); ?>
            </div>
                         
            <div class="" style="margin-top:20px;margin-bottom: 20px;">                  
                <?= Html::submitButton("Filtlrar", ["class" => "btn btn-primary", "style"=>"width:270px;font-size:20px"]) ?>  
            </div> 
            
            <div class="" style="margin-top:25px;">
                <h3>Campos Rematriculación</h3>
                <div class="form-group" style="margin-top:15px;">  
                    <label>Curso Académico</label>
                    <?= Html::dropDownList('Curso Academico','', $curso_acad,['class' => 'form-control','prompt'=>'','id'=>'cursoacad_re']) ?>
                </div>
                <div class="form-group">
                      <label>Ciclo Formativo</label>
                     <?= Html::dropDownList('Ciclo Formativo','', $ciclo,['class' => 'form-control','prompt'=>'','id'=>'ciclo_re']) ?>
                </div>
                <div class="form-group">
                    <label>Curso</label>
                     <?= Html::dropDownList('Curso','', $curso,['class' => 'form-control','prompt'=>'','id'=>'curso_re']) ?>
                </div>

                <div class="" style="margin-top:20px;margin-bottom: 20px;">                  
                   <?= Html::Button("Rematricular", ["class" => "btn", "style"=>"width:270px;font-size:20px", "id"=>"ver"]) ?>  
                </div>
            </div>    

        </div>

        
      <?php ActiveForm::end(); ?>          
 
        <div class="col col-sm-9" style="width: 850px;">
<!--            <div id ="campos_destino_matricula" class="form-group">
                <label>Curso Académico</label>
                <input id="curso_acad_dest" placeholder=""/>
            </div>
            <div id ="" class="form-group">
                <label>Ciclo Formativo</label>
                <input id="ciclo_dest" placeholder=""/>
            <div id ="" class="form-group">    
                <label>Curso</label>
                <input id="curso_dest" placeholder="Nuevo Curso" value="2"/>
                
            </div>
             <div id ="boton_rematricular" class="form-group">    -->

     
            
            
        <div id="contenedor_grid" style="height: 700px;overflow-y: scroll;font-size: 12px;">      
                   
<?php
    if(isset($datos)){
    
?> 
           <?= GridView::widget([
             'dataProvider' => $datos,
             'summary' => '',
             'id' => 'grid',
           
             //'filterModel' => $searchModel,
             'columns' => [
                'dni',
                'nombre',
                'apellidos',
                'pago',
                'banco',
                'tipo',
                'seguro',
                 [
                     'class' => 'yii\grid\checkboxColumn',
                     'name' => 'sel'
                 ],
             ],
               
               
            ]);
    }?>
       
              
        </div>

        <div id="mensaje" style="margin-top:10px;display:none">
            <div class="alert alert-warning alert-dismissible mb-2" role="alert" style="text-align: center">
<!--                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>-->
                <strong> Alumnos Rematriculados con éxito</strong>
            </div>
        </div>
 
        </div>
    </div>

</div> 


<script>
    $( document ).ready(function() {
         $(document).on("click", "#ver", function () {
            ver_seleccionados();
    });  
   
    
        //al cambiar el curso academico de filtro se cambia el de rematriculacion +1
       if ($( "#filtros_rematriculas" ).unbind('submit')) {
            var content_cursoacad = $("#curso_acad option:selected").val();
            var nuevo_cursoacad = parseInt($("#curso_acad option[value="+ content_cursoacad  +"]").val()) + parseInt('1');           
            var asign_nuevo_curso = nuevo_cursoacad +"-"+ parseInt(nuevo_cursoacad +1);
            var content_ciclo = $("#ciclo option:selected").val();
            var content_curso = $("#curso option:selected").val();
            var nuevo_curso = parseInt($("#curso option[value="+ content_curso +"]").val()) + parseInt('1');

            $("#cursoacad_re").val(asign_nuevo_curso);
            $('#ciclo_re').val(content_ciclo);
            $('#curso_re').val(nuevo_curso);
             
            
         };  
   
});   
   
    function ver_seleccionados(){
        //guardamos los ids del gridview seleccionados 
        var datos = $('#grid').yiiGridView('getSelectedRows');
      
        //console.log(datos);
        //array que contendrá el primer array de filtrado
        var arr = [];
        //array que contendrá el array final para enviar a la action
        var_arr_sel =[];
        //variable que contiene todo el html del grid
        var $table = $('#grid');
        //array que guardará mediante el find los encabezados o titulos de las columnas
        var columnNames = $table.find('thead th').map(function() {        
            return $(this).text();
        });
    //recorremos todos los tr de la tabla
    $table.find('tr').each(function(){
        var rowValues = {};
        //por cada td que encontramos guardamos la columna nombre del dato con su correspondiente texto del td
        $(this).find('td').each(function(i) {
            rowValues[columnNames[i]] = $(this).text();
        });
        //vamos generando el array con todo el contenido
        arr.push(rowValues);
        
    });
       //recorremos el primer array que contiene los id seleccionados y le sumamos uno para evitar el encabezado
       for (let value of datos) {
           value ++
           for(let c = 0;c < arr.length; c++){
               //cuando coincida el id inicial seleccionado con el id del array que contiene todos los datos se guarda en un nuevo array que será el definitivo a enviar a la action
               if (c == value){
                   var_arr_sel.push(arr[c]);      
               }
           }
 
        }
        
        
       
        
        enviar_sel(var_arr_sel);
    }
    function enviar_sel(datos){    
       //console.log($("#curso :selected").val());

            var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
            var cursoacad = $("#cursoacad_re").val();
            var nuevo_ciclo = $("#ciclo_re").val();
            var nuevo_curso = $("#curso_re").val();
              
        $.ajax({

            type: "POST",
            
            url: baseUrl + "/matriculas/rematriculando",

            data: {'datos': JSON.stringify(datos),
                    'nuevo_curso_academico':cursoacad,
                    'nuevo_ciclo':nuevo_ciclo,
                    'nuevo_curso':nuevo_curso,
                    },     
            
            success: function(data){
                console.log(data);
               if (data == true){
                   for(let i=0;i<10;i++){
                       $("#mensaje").fadeToggle(1000); 
                   }
                   
               }
               
            }
         });
    }
</script>