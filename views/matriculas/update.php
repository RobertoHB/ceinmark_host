<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Matriculas */

//$this->title = 'Actualizando Matricula: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Matriculas', 'url' => ['index','alumno' => $_REQUEST['alumno']]];
$this->params['breadcrumbs'][] = ['label' => 'Alumno', 'url' => ['alumnos/update','id'=>$_REQUEST['alumno']]];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizando Matricula Nº '.$model->id;

?>
<div class="matriculas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'datosAlumno'=>$datosAlumno,
        'datosModulo' => $datosModulo,
        'modelModulos'=>$modeloModulos,
        'modelDatosBancarios' => $modeloDatosBancarios,
    ]) ?>

</div>
