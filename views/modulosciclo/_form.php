<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Ciclos;
use app\models\Modulos;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Modulosciclo */
/* @var $form yii\widgets\ActiveForm */

$itemCiclos = ArrayHelper::map(Ciclos::find()->all(), 'id', 'denominacion');
$itemModulos = ArrayHelper::map(Modulos::find()->all(), 'id', 'nombre');

?>

<div class="modulosciclo-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <!--$form->field($model, 'id_ciclo')->textInput()--> 
   
    <?= $form->field($model, 'id_ciclo')->dropDownList($itemCiclos, ['prompt' => 'Seleccione Uno' ])->label('Ciclo'); ?>

    

     <!--$form->field($model, 'id_modulo')->textInput()--> 
    
    <?= $form->field($model, 'id_modulo')->dropDownList($itemModulos, ['prompt' => 'Seleccione Uno' ])->label('Modulo'); ?>

    <?= $form->field($model, 'curso')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
