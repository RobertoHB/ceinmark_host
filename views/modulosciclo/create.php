<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Modulosciclo */

$this->title = 'Nuevo Modulo/Ciclo';
$this->params['breadcrumbs'][] = ['label' => 'Modulosciclos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modulosciclo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
