<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Ciclos;
use app\models\Modulos;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ModuloscicloSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Modulos/Ciclos';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<div class="modulosciclo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Modulos/Ciclos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            
            [
            'attribute' => 'id',
            'contentOptions'=>[ 'style'=>'width: 75px'],
            ],    
            //'id_ciclo',
            [
                'label' => 'Ciclo',
                'attribute' => 'id_ciclo',
                'value' => 'ciclo0.denominacion',
                'filter' => ArrayHelper::map(Ciclos::find()->all(), 'id', 'denominacion'),
                'enableSorting' => true,
            ],


            //'id_modulo',
            [
                'label' => 'Modulo',
                'attribute' => 'id_modulo',
                'value' => 'modulo0.nombre',
                'filter' => ArrayHelper::map(Modulos::find()->all(), 'id', 'nombre'),
                'enableSorting' => true,
            ],
            
            
            [
                'attribute' => 'curso',
                'contentOptions'=>[ 'style'=>'width: 75px'],
            ],    


//            [
//                'class' => 'yii\grid\ActionColumn',
//                'contentOptions'=>[ 'style'=>'width: 80px'], 
//            ],
            ['class' => 'yii\grid\ActionColumn',
                
                'contentOptions' => ['style' => 'width:40px;'],
                'header'=>'',
                'template' => '{actualizar}',
                'buttons' => [
                   'actualizar' => function ($url, $model, $key) {
                                                return  Html::a(
                                                '<span class="glyphicon glyphicon-pencil" style="padding-left:5px;"></span>',['modulosciclo/update', 'id' =>$model->id],
                                                  //Url::to('@web/modulosciclo/update'.'?id='.$model->id),
                                                 ['title' => Yii::t('app', 'Editar Módulo')],                                                        
                                                        
                                                        );
                    },
                ], 
            ],
            ['class' => 'yii\grid\ActionColumn',
                
                'contentOptions' => ['style' => 'width:115px;'],
                'header'=>'',
                'template' => '{circulares}{asistencia}{cuentasbancarias}{etiquetas}',
                'buttons' => [
//                   'actualizar' => function ($url, $model, $key) {
//                                                return  Html::a(
//                                                '<span class="glyphicon glyphicon-pencil" style="padding-left:5px;"></span>',Url::to('@web/modulosciclo/update'.'?id='.$model->id),
//                                                 ['title' => Yii::t('app', 'Editar Módulo')],                                                        
//                                                        
//                                                        );
//                    },
                    'circulares' => function ($url, $model, $key) {
                            return  Html::a(
                            '<span class="fas fa-mail-bulk" style="padding-left:5px;"></span>',
                             // Url::to(
                                    ['modulosciclo/circulares','ciclo' => $model->id_ciclo,'modulo' => $model->id_modulo,'curso' => $model->curso],                                                               
                                    ['title' => Yii::t('app', 'Envio circulares')],
                            );
                    },
                    'asistencia' => function ($url, $model, $key) {
                            return  Html::a(
                            '<span class="fa fa-users" style="padding-left:5px;"></span>',
                                    ['modulosciclo/asistencia','ciclo' => $model->id_ciclo,'modulo' => $model->id_modulo,'curso' => $model->curso],
                                    ['title' => Yii::t('app', 'Control Asistencia'),'target'=>'_blank'],                                                                

                            );
                    },  
                    'cuentasbancarias' => function ($url, $model, $key) {
                            return  Html::a(
                            '<span class="fas fa-money-check-alt" style="padding-left:5px;"></span>',
                                    ['modulosciclo/cuentas_bancarias','ciclo' => $model->id_ciclo,'modulo' => $model->id_modulo,'curso' => $model->curso],
                                    ['title' => Yii::t('app', 'Cuentas Bancarias (remesa)')],                                                                

                            );
                    },   
                    'etiquetas' => function ($url, $model, $key) {
                            return  Html::a(
                            '<span class="fas fa-tags" style="padding-left:5px;"></span>',
                                    ['modulosciclo/etiquetas','ciclo' => $model->id_ciclo,'modulo' => $model->id_modulo,'curso' => $model->curso],
                                    ['title' => Yii::t('app', 'Generar etiquetas'),'target'=>'_blank'],                                                                

                            );
                    },  
                ]
            
            ],
            
        ],
    ]); ?>


</div>
